package com.kashyap.matrimony.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.kashyap.matrimony.R;
import com.kashyap.matrimony.TimePicker;
import com.kashyap.matrimony.adapter.CityAdapter;
import com.kashyap.matrimony.adapter.LanguageAdapter;
import com.kashyap.matrimony.database.tables.UserCityTbl;
import com.kashyap.matrimony.database.tables.UserLanguageTbl;
import com.kashyap.matrimony.database.tables.UserTbl;
import com.kashyap.matrimony.model.UserCityModel;
import com.kashyap.matrimony.model.UserLanguageModel;
import com.kashyap.matrimony.model.UserModel;
import com.kashyap.matrimony.util.Constant;
import com.kashyap.matrimony.util.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.kashyap.matrimony.util.Utils.getDateFromString;

public class AddCandidateActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etFatherName)
    EditText etFatherName;
    @BindView(R.id.etSurName)
    EditText etSurName;
    @BindView(R.id.rbMale)
    RadioButton rbMale;
    @BindView(R.id.rbFemale)
    RadioButton rbFemale;
    @BindView(R.id.etDOB)
    EditText etDOB;
    @BindView(R.id.etEmailAddress)
    EditText etEmailAddress;
    @BindView(R.id.etPhoneNumber)
    EditText etPhoneNumber;
    @BindView(R.id.spCity)
    Spinner spCity;
    @BindView(R.id.spLanguage)
    Spinner spLanguage;
    @BindView(R.id.chbActCricket)
    CheckBox chbActCricket;
    @BindView(R.id.chbActFootBall)
    CheckBox chbActFootBall;
    @BindView(R.id.chbActHockey)
    CheckBox chbActHockey;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    Calendar getDob = null;
    Calendar todayDate = null;
    String dateParts[];


    UserCityTbl userCityTbl;

    CityAdapter cityAdapter;
    LanguageAdapter languageAdapter;

    ArrayList<UserCityModel> cityList = new ArrayList<>();
    ArrayList<UserLanguageModel> languageList = new ArrayList<>();

    UserModel userModel;

    String startingDate = "1990-08-10T11:49:00";

    LocalDate getDateData;

    String age;
    @BindView(R.id.etDOBTime)
    EditText etDOBTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_candidate);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_add_candidate), false);

        userCityTbl = new UserCityTbl(this);

        bindValues();
        setDateToView();
        setSpinnerAdapter();
        getDataForUpdate();
        setTime();
    }

    void getDataForUpdate() {
        if (getIntent().hasExtra(Constant.USER_OBJECT)) {
            userModel = (UserModel) getIntent().getSerializableExtra(Constant.USER_OBJECT);
            getSupportActionBar().setTitle(R.string.lbl_edit_candidate);
            etName.setText(userModel.getUserName());
            etFatherName.setText(userModel.getUserFatherName());
            etSurName.setText(userModel.getUserSurname());

            etPhoneNumber.setText(userModel.getPhoneNumber());
            etDOB.setText(userModel.getUserDob());
            if (userModel.getUserGender() == Constant.MALE) {
                rbMale.setChecked(true);
            } else {
                rbFemale.setChecked(true);
            }
            etEmailAddress.setText(userModel.getUserEmailAddress());
            etDOBTime.setText(userModel.getUserDobTime());
            spCity.setSelection(getSelectedPositionFromCityId(userModel.getUserCityId()));
            spLanguage.setSelection(getSelectedPositionFromLanguageId(userModel.getUserLanguageId()));

            String hobbies = userModel.getUserHobbies();

            String[] hobby = hobbies.split(",");

            for (String w : hobby) {
                if (w.equals(chbActCricket.getText().toString())) {
                    chbActCricket.setChecked(true);
                }
                if (w.equals(chbActHockey.getText().toString())) {
                    chbActHockey.setChecked(true);
                }
                if (w.equals(chbActFootBall.getText().toString())) {
                    chbActFootBall.setChecked(true);
                }
            }
        }
    }

    int getSelectedPositionFromCityId(int cityId) {
        for (int i = 0; i < cityList.size(); i++) {
            if (cityList.get(i).getUserCityId() == cityId) {
                return i;
            }
        }
        return 0;
    }

    int getSelectedPositionFromLanguageId(int languageId) {
        for (int i = 0; i < languageList.size(); i++) {
            if (languageList.get(i).getUserLanguageId() == languageId) {
                return i;
            }
        }
        return 0;
    }


    void setDateToView() {
        final Calendar newCalendar = Calendar.getInstance();
        etDOB.setText(
                String.format("%02d", newCalendar.get(Calendar.DAY_OF_MONTH)) + "/" +
                        String.format("%02d", newCalendar.get(Calendar.MONTH) + 1) + "/" +
                        String.format("%02d", newCalendar.get(Calendar.YEAR)));
    }

    void setSpinnerAdapter() {
        cityList = userCityTbl.getCityList();

        //Toast.makeText(this, ""+cityList.size(), Toast.LENGTH_SHORT).show();
        languageList.addAll(new UserLanguageTbl(this).getLanguageList());
        cityAdapter = new CityAdapter(this, cityList);
        languageAdapter = new LanguageAdapter(this, languageList);

        spCity.setAdapter(cityAdapter);

        spLanguage.setAdapter(languageAdapter);
    }


    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        etDOB.setText(String.format("%02d", day) + "/" + String.format("%02d", (month + 1)) + "/" + year);
    }

    void bindValues() {

        etDOBTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment timePicker = new TimePicker();
                timePicker.show(getSupportFragmentManager(),"time picker");
            }
        });

        etDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar newCalendar = Calendar.getInstance();
                Date date = getDateFromString(startingDate);
                newCalendar.setTimeInMillis(date.getTime());
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        AddCandidateActivity.this, AddCandidateActivity.this,
                        newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View view) {
                if (isValidUser()) {
                    if (userModel == null) {

                        String hobbies = "";
                        if (chbActCricket.isChecked()) {
                            hobbies += "," + chbActCricket.getText().toString();
                        }
                        if (chbActFootBall.isChecked()) {
                            hobbies += "," + chbActFootBall.getText().toString();
                        }
                        if (chbActHockey.isChecked()) {
                            hobbies += "," + chbActHockey.getText().toString();
                        }

                        long lastInsetedID = new UserTbl(AddCandidateActivity.this).insertUserById(etName.getText().toString(),
                                etFatherName.getText().toString(), etSurName.getText().toString(), rbMale.isChecked() ? Constant.MALE : Constant.FEMALE,
                                hobbies.substring(1), Utils.getFormatedDateToInsert(etDOB.getText().toString()),
                                etEmailAddress.getText().toString(),
                                etPhoneNumber.getText().toString(),
                                languageList.get(spLanguage.getSelectedItemPosition()).getUserLanguageId(),
                                cityList.get(spCity.getSelectedItemPosition()).getUserCityId(), 0,etDOBTime.getText().toString());
                        if (lastInsetedID > 0) {
                            Toast.makeText(AddCandidateActivity.this, "User Inserted Successfully", Toast.LENGTH_SHORT).show();
                            Intent addCandidateIntent = new Intent(AddCandidateActivity.this, CandidateListByGenderActivity.class);
                            startActivity(addCandidateIntent);


                        } else {
                            Toast.makeText(AddCandidateActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        String hobbies = "";
                        if (chbActCricket.isChecked()) {
                            hobbies += "," + chbActCricket.getText().toString();
                        }
                        if (chbActFootBall.isChecked()) {
                            hobbies += "," + chbActFootBall.getText().toString();
                        }
                        if (chbActHockey.isChecked()) {
                            hobbies += "," + chbActHockey.getText().toString();
                        }

                        long lastUpdatedID = new UserTbl(AddCandidateActivity.this).updateUserById(userModel.getUserId(), etName.getText().toString(),
                                etFatherName.getText().toString(), etSurName.getText().toString(), rbMale.isChecked() ? Constant.MALE : Constant.FEMALE,
                                hobbies.substring(1), Utils.getFormatedDateToInsert(etDOB.getText().toString()),
                                etEmailAddress.getText().toString(),
                                etPhoneNumber.getText().toString(),
                                languageList.get(spLanguage.getSelectedItemPosition()).getUserLanguageId(),
                                cityList.get(spCity.getSelectedItemPosition()).getUserCityId(), userModel.getIsFavorite(),etDOBTime.getText().toString());

                        if (lastUpdatedID > 0) {
                            Toast.makeText(AddCandidateActivity.this, "User Updated", Toast.LENGTH_SHORT).show();
                            Intent addCandidateIntent = new Intent(AddCandidateActivity.this, CandidateListByGenderActivity.class);
                            startActivity(addCandidateIntent);

                        } else {
                            Toast.makeText(AddCandidateActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    boolean isValidUser() {
        boolean isValid = true;

        if (TextUtils.isEmpty(etName.getText().toString())) {
            isValid = false;
            etName.setError(getString(R.string.lbl_name));
        }

        if (TextUtils.isEmpty(etFatherName.getText().toString())) {
            isValid = false;
            etFatherName.setError(getString(R.string.error_father_name));
        }

        if (TextUtils.isEmpty(etSurName.getText().toString())) {
            isValid = false;
            etSurName.setError(getString(R.string.error_enter_surname));
        }

        if (TextUtils.isEmpty(etPhoneNumber.getText().toString())) {
            isValid = false;
            etPhoneNumber.setError(getString(R.string.error_enter_phone));
        } else if (etPhoneNumber.getText().toString().length() < 10) {
            isValid = false;
            etPhoneNumber.setError(getString(R.string.error_valid_phone));
        }

        if (TextUtils.isEmpty(etEmailAddress.getText().toString())) {
            isValid = false;
            etEmailAddress.setError(getString(R.string.error_ente_email));
        } else if (!Patterns.EMAIL_ADDRESS.matcher(etEmailAddress.getText().toString()).matches()) {
            isValid = false;
            etEmailAddress.setError(getString(R.string.error_valid_email));
        }

        String dateParts[] = etDOB.getText().toString().split("/");
        Integer age = Integer.parseInt(Utils.getAge(Integer.parseInt(dateParts[2]), Integer.parseInt(dateParts[1]), Integer.parseInt(dateParts[0])));


        if (age < 0) {
            isValid = false;
            etDOB.setError("");
            Toast.makeText(this, getString(R.string.lbl_enter_valid_date), Toast.LENGTH_SHORT).show();
            etDOB.requestFocus();
        }


        if (!(chbActCricket.isChecked() || chbActFootBall.isChecked() || chbActHockey.isChecked())) {
            Toast.makeText(this, "Please Select any one Checkbox", Toast.LENGTH_LONG).show();
            isValid = false;
        }
        return isValid;
    }

    @Override
    public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {
        etDOBTime.setText(String.format("%02d",hourOfDay)+":"+String.format("%02d",minute));
    }

    public void setTime(){
        Date date = new Date();
        DateFormat formatter = new SimpleDateFormat("HH:mm"); //Hourse:Minutes
        String dateFormatted = formatter.format(date); //string representation
        etDOBTime.setText(dateFormatted);
    }
}